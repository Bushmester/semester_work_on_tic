import sys

from run_codes.run_bwt_lzw import run_bwt_lzw_decode
from run_codes.run_shanon import run_shanon_encode
from utils.serializer.get_data import get_lines_from_file


def main():
    strategies = {
        "shanon_encode": run_shanon_encode,
        "bwt_lzw_decode": run_bwt_lzw_decode
    }

    strategy = sys.argv[1]
    file_name = input("Enter file name: ")

    lines = get_lines_from_file(file_name)

    output = strategies[strategy](lines)
    print(output)


if __name__ == "__main__":
    main()
