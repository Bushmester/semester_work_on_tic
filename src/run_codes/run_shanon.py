from shanon.encoder import encode


def run_shanon_encode(lines: list) -> str:
    string_for_encode = input("Enter string for encode: ")
    alphabet = lines[0].split()
    probabilities = list(map(float, lines[1].split()))
    shanon_encode = encode(alphabet, probabilities)

    result = ''
    for string in string_for_encode:
        result += shanon_encode[string] + ' '

    return f"Your encoded text: {result}"
