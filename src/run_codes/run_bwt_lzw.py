from bwt.decoder import bwt_decoder
from utils.serializer.bin_to import bin_to_str
from utils.serializer.get_data import get_compressed_data
from lzw.decoder import lzw_decoder


def run_bwt_lzw_decode(lines: list) -> str:
    alphabet = lines[0].split()

    bin_data = input("Enter encoded and compressed text (in binary): ")
    line_number = input("Enter line number for BWT algorithm (in binary): ")

    str_data = bin_to_str(bin_data)
    str_data = get_compressed_data(str_data)
    lzw = lzw_decoder(alphabet, str_data)

    line_number = bin_to_str(line_number)
    n = len(lzw)
    matrix = ['' for _ in range(n)]
    bwt_decoder(lzw, n, matrix)
    decoded_string = matrix[int(line_number)]

    return f"Your decoded and compressed text: {decoded_string}"
