def float_to_bin(value: float, place: int) -> str:
    result = ""

    _, value = str(value).split(".")

    for _ in range(place):
        value = float(f"0.{value}") * 2
        res, value = str(value).split(".")
        result += res

    return result
