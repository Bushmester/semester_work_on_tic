def bin_to_str(bin_data: str) -> str:
    bin_data = bin_data.split(" ")
    str_data = [chr(int(bin_el, 2)) for bin_el in bin_data]
    return ''.join(str_data)
