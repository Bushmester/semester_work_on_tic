def get_compressed_data(string_compressed_data: str) -> list[int]:
    compressed_data = string_compressed_data.split("^")
    compressed_data = list(map(int, compressed_data))

    return compressed_data


def get_lines_from_file(file_name: str) -> list:
    lines = []
    with open(file_name, 'r') as f:
        for line in f.readlines():
            lines.append(line)

    lines = [line.replace('\n', '') for line in lines]

    return lines

