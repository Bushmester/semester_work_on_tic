def bwt_decoder(string: str, n: int, matrix: list) -> None:
    for index, char in enumerate(string):
        matrix[index] = char + matrix[index]

    matrix.sort()

    if len(matrix[0]) == n:
        return

    bwt_decoder(string, n, matrix)
