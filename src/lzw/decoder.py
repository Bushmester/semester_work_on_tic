def lzw_decoder(dictionary: list[str], compressed_data: list[int]) -> str:
    result = dictionary[compressed_data[0]]
    current_line = ''

    for code in compressed_data:
        substring = dictionary[code]

        current_line += substring[0]
        if current_line in dictionary:
            continue
        dictionary.append(current_line)

        current_line = substring
        result += current_line

    return result
