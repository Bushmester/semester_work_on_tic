from math import ceil, log2

from utils.serializer.float_to import float_to_bin


def encode(alphabet: list[str], probabilities: list[float]) -> dict[str, str]:
    dictionary = dict()

    for letter, p in zip(alphabet, probabilities):
        dictionary[letter] = p

    dictionary = dict(
        sorted(dictionary.items(), key=lambda item: item[1], reverse=True)
    )
    dictionary_items = dictionary.items()

    sorted_alphabet = [letter for letter, _ in dictionary_items]
    sorted_probabilities = [p for _, p in dictionary_items]
    sum_array = []

    for i, letter in enumerate(sorted_alphabet):
        if i == 0:
            sum_array.append(float(0))
            continue

        sum_array.append(sum_array[-1] + sorted_probabilities[i - 1])

    log_array = list(map(ceil, [-1 * value for value in list(map(log2, sorted_probabilities))]))
    bin_array = [float_to_bin(sum_item, log) for sum_item, log in zip(sum_array, log_array)]

    shanon_encode_values = {key: value for key, value in zip(sorted_alphabet, bin_array)}

    return shanon_encode_values
